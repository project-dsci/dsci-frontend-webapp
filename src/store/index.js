import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        authenticated: '',
        token: localStorage.getItem('token') || '',
        user: {
            name: null,
            role: null
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.authenticated,
        getUserRole: state => state.user.role,
        getUserName: state => state.user.name,
    },
    mutations: {
        auth_request(state) {
            state.authenticated = 'loading'
        },
        auth_success(state, user) {
            state.authenticated = 'success'
            state.user.name = user.getName()
            state.user.role = user.getRole()
            state.token = localStorage.getItem('token') || ''
        },
        auth_error(state) {
            state.authenticated = 'error'
        },
        logout(state) {
            state.authenticated = ''
            state.token = ''
        },
    },
    actions: {},
    modules: {}
})
