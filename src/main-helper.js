//Bootstrap vue
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'

//Scss
import '@/assets/scss/style.scss'

//Validation form
import {ValidationObserver} from "vee-validate";
import {ValidationProvider} from "vee-validate";
import './services/vee-validate-rules-service'

//Constant
import {Constants} from '@/services/constant-service'

export default {
    install(Vue) {
        Vue.use(BootstrapVue);
        Vue.use(IconsPlugin);
        Vue.use(Constants)
        Vue.component('ValidationObserver', ValidationObserver);
        Vue.component('ValidationProvider', ValidationProvider);
    }
};
