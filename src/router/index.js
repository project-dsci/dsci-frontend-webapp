import Vue from 'vue'
import VueRouter from 'vue-router'
import ChiefCandidateView from "@/views/chief/ChiefCandidateView";
import ChiefFormationView from "@/views/chief/ChiefFormationView";
import LoginView from "@/views/LoginView";
import ErrorView from "@/views/ErrorView";
import store from '@/store'
import {Constants} from "@/services/constant-service";
import Dashboard from "@/views/components/Dashboard";
import {logout} from "@/services/auth-service";
import Layout from "@/views/components/Layout";

Vue.use(VueRouter)

const routes = [
    {
        path: '/login',
        name: 'LoginView',
        component: LoginView,
    },
    {
        path: '',
        name: 'Layout',
        component: Layout,
        meta: {requireAuth: true},
        children: [
            // ======= ChiefViews =======
            {
                path: '/dashboard',
                name: 'DashboardView',
                component: Dashboard,
            },
            {
                path: '/candidats',
                name: 'ChiefCandidateView',
                component: ChiefCandidateView,
                meta: {requireAuthWithRole: [Constants.CHIEF]}
            },
            {
                path: '/formations',
                name: 'ChiefFormationView',
                component: ChiefFormationView,
                meta: {requireAuthWithRole: [Constants.CHIEF]}
            },
            // ======= candidateViews =======
        ]
    },
    // ======= OtherViews =======
    {
        path: '*',
        name: 'ErrorView',
        component: ErrorView,
    }
]

const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {

    const authenticated = store.getters.isLoggedIn;
    const userRole = store.getters.getUserRole;

    if (to.name === "LoginView" && authenticated) next(from)

    if (to.matched.some(m => m.meta.requireAuth) && !authenticated) next('/login')

    if (to.meta.requireAuthWithRole && !to.meta.requireAuthWithRole.includes(userRole)) next(from)

    if (authenticated && userRole == null) logout()

    next()

})

export default router
