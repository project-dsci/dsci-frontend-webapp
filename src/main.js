import Vue from 'vue'
import App from './App.vue'
import MainHelper from './main-helper';
import router from './router'
import store from './store'

Vue.config.productionTip = false

Vue.use(MainHelper)

new Vue({
    router: router,
    store,
    render: h => h(App)
}).$mount('#app')
