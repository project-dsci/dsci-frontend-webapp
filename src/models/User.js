export default class User {

    email;
    password;
    role;
    firstname;
    lastname;

    constructor(email, password, role, firstname, lastname) {
        this.email = email;
        this.password = password;
        this.role = role;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    getRole() {
        return this.role;
    }

    getName() {
        if (this.firstname != null && this.lastname != null) {
            return this.firstname + ' ' + this.lastname;
        }

        return this.email;
    }
}
