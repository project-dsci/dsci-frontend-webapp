import User from "@/models/User"
import {Constants} from "@/services/constant-service";
import store from "@/store"
import router from "@/router"

const users = [];
users.push(new User("chef@gmail.com", "chef", Constants.CHIEF, "Lauren", "Fiacre"))
users.push(new User("candidat@gmail.com", "candidat", Constants.CANDIDATE, "Jane", "Doe"))

/**
 * Login the user to the application
 * @param email - Email of the user user
 * @param password - Password of the user
 * @returns An error message if the identifiers are incorrect
 */
function login(email, password) {

    let user = users.find(user => user.email === email && user.password === password);

    if (user == null) {
        store.commit('auth_error')
        return "Le couple identifiant/mot de passe est invalide.";
    }

    new Promise((resolve, reject) => {
        const token = "163937294";
        localStorage.setItem('token', token);
        store.commit('auth_success', user);
        router.push('/dashboard')
            .then(resolve)
            .catch(reject)
    })
        .catch(() => {
            store.commit('auth_error');
            localStorage.removeItem('token')
        })

}

/**
 * Logout the user to the application
 */
function logout() {

    new Promise((resolve, reject) => {
        store.commit('logout')
        localStorage.removeItem('token')
        router.push({name: "LoginView"})
            .then(resolve)
            .catch(reject);
    }).catch(error => {
        console.log(error)
    })

}

export {login, logout}
