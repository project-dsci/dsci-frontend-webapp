import {extend} from "vee-validate";
import {email, required, alpha_num, alpha_spaces, min, max} from "vee-validate/dist/rules";

extend('required', {
    ...required,
    message: 'Le champs ne peut pas être vide.'
});
extend('email', {
    ...email,
    message: 'L\'adresse mail n\'est pas valide.'
});

extend('alpha_num', {
    ...alpha_num,
    message: 'Le texte ne doit pas contenir d\'espace ou de caractères spéciaux.'
});

extend('alpha_spaces', {
    ...alpha_spaces,
    message: 'Le texte ne doit pas contenir de caractères spéciaux.'
});

extend('min', {
    ...min,
    message: 'Le texte doit être supérieur à {length} caractères.'
});

extend('max', {
    ...max,
    message: 'Le texte doit être inférieur à {length} caractères.'
});

// Custom rules
extend('required-password', {
    ...required,
    message: 'Le mot de passe est requis.'
});
